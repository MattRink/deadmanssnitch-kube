package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/joho/godotenv"
	"gopkg.in/yaml.v2"
)

// Config for the app.
type Config struct {
	Hosts []struct {
		Name string `yaml:"name"`
		Key  string `yaml:"key"`
	}
}

func makeRequests(ctx context.Context, exitChan chan struct{}, url *url.URL, interval int) {
	client := &http.Client{}

	req, _ := http.NewRequest("GET", url.String(), nil)

	for {
		log.Println(fmt.Sprintf("Reaching out to %s\n", url.String()))

		resp, err := client.Do(req)

		if err != nil {
			log.Println("Error while making request: " + err.Error())
		}

		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			log.Println("Non-2xx status code received: " + resp.Status)
		}

		time.Sleep(time.Duration(interval) * time.Second)

		select {
		case <-ctx.Done():
			fmt.Println("Received done, exiting in 500 milliseconds")
			time.Sleep(500 * time.Millisecond)
			exitChan <- struct{}{}
			return
		default:
		}
	}
}

func main() {
	godotenv.Load()
	hostname := os.Getenv("HOSTNAME")

	if hostname == "" {
		log.Println("HOSTNAME is empty. Unable to proceed")
		os.Exit(-1)
	}

	log.Println("Starting deadmanssnitch for " + hostname)

	f, err := os.Open("/etc/deadmanssnitch/config.yaml")
	if err != nil {
		log.Println("Unable lo load config: " + err.Error())
		os.Exit(-1)
	}
	defer f.Close()

	var cfg Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		log.Println("Unable to read config: " + err.Error())
		os.Exit(-1)
	}

	log.Printf("Found config with %d hosts\n", len(cfg.Hosts))

	interval, err := strconv.Atoi(os.Getenv("DEADMANSSNITCH_INTERVAL"))

	if err != nil {
		log.Println("Unknown or unset interval, setting to 60.")
		interval = 60
	}

	key := ""

	for i := 0; i < len(cfg.Hosts); i++ {
		if hostname == cfg.Hosts[i].Name {
			log.Println("Found config for host.")
			key = cfg.Hosts[i].Key
			break
		}
	}

	if key == "" {
		log.Println("Key is empty. Unable to proceed")
		os.Exit(-1)
	}

	url := &url.URL{
		Scheme: "https",
		Host:   "nosnch.in",
		Path:   key,
	}

	ctx, cancel := context.WithCancel(context.Background())
	exitChan := make(chan struct{})

	go makeRequests(ctx, exitChan, url, interval)

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		select {
		case <-signalChan:
			log.Println("Exit signal recieved.")
			cancel()
			return
		default:
		}
	}()
	<-exitChan

	log.Println("Cleaning up...")
}
