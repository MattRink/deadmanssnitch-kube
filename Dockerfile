FROM golang:1.17-bullseye

WORKDIR /app

COPY . /app

RUN go build -v -o deadmanssnitch-kube .

CMD [ "/app/deadmanssnitch-kube" ]
